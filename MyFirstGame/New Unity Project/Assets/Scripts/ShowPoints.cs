﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPoints : MonoBehaviour
{
    public void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 40), "" + GameManager.Instance.points);
    }

}
